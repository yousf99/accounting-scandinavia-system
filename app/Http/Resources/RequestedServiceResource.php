<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RequestedServiceResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service' => new ServiceResource($this->Service),
            'price' => $this->price,
            'description' => $this->description,
            'duration' => $this->duration,
            'duration_type' => $this->duration_type,
            'start_date' => $this->start_date,
        ];
    }
}
