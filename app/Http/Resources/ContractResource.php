<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractResource extends JsonResource
 {
    public function toArray( $request )
 {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date'=>$this->updated_at,
            'pdf'=>$this->getFileContractPath(),
            'client' => new ClientResource( $this->client ),

        ];
    }
}
