<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Service;

class RequestedClientServicesResource extends JsonResource {
    /**
    * Transform the resource into an array.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
    */

    public function getService( $id ) {
        $service = Service::find( $id );
        return new ServiceResource( $service );
    }

    public function toArray( $request ) {
        return [
            'id'=>$this->id,
            'service' =>$this->getService( $this->service_id ),
            'price' => $this->price,
            'description' => $this->description,
            'duration' => $this->duration,
            'duration_type' => $this->duration_type,
            'start_date' => $this->start_date,
        ];
    }
}
