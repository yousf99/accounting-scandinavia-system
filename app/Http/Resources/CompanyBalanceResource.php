<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyBalanceResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'company_info' => new CompanyResource($this),
            'payments' => PaymentResource::collection($this->Payments),
            'total_payments' => $this->total_payments,
        ];
    }
}
