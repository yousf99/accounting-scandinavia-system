<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
 {
    public function toArray( $request )
 {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'fax' => $this->fax,
            'company' => new CompanyResource( $this->Company ),
            'country' => new CountryResource( $this->Country ),
        ];
    }
}
