<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProposalResource extends JsonResource
 {
    public function toArray( $request )
 {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date'=>$this->created_at,
            'pdf'=>$this->getFilePath(),
            'status' => ( $this->approved === null ) ? '1' : '0',
            'reason' => $this->reason,
            'client' => new ClientResource( $this->client ),

        ];
    }
}
