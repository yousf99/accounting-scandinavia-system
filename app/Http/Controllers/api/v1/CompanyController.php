<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Company;
use App\Models\Client;
use App\Models\Contract;
use App\Models\Payment;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\CompanyBalanceResource;
use App\Http\Resources\ContractResource;
use App\Http\Resources\PaymentResource;

class CompanyController extends ApiController
 {

    public function index() {
        $company = Company::all();
        return $this->apiResponse( CompanyResource::collection( $company ), ApiController::STATUS_OK, 'companies have been retrieved successfully' );
    }

    public function show( $id ) {
        $company = Company::find( $id );
        if ( $company ) {
            return $this->apiResponse( new CompanyResource( $company ), ApiController::STATUS_OK, 'Company have been retrieved successfully' );
        }
        return $this->apiResponse( '', ApiController::STATUS_NOT_FOUND, 'There are no data' );
    }

    public function store( Request $request ) {
        $validation = $this->apiValidation( $request, [
            'name' => 'required|unique:companies,name',
        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }
        $user_id = auth( 'sanctum' )->user()->id;
        $company = Company::create( array_merge( $request->only( [
            'name'
        ] ), [ 'user_id' => $user_id ] ) );
        $accountNo = 'C'. str_pad( ( string )$company->id, 4, '0', STR_PAD_LEFT );
        $company->account_no = $accountNo;
        $company->update();
        return $this->apiResponse( new CompanyResource( $company ), ApiController::STATUS_CREATED, 'Added successfully' );
    }

    public function update( Request $request, $id ) {
        $company = Company::find( $id );
        if ( $company ) {
            $validation = $this->apiValidation( $request, [
                'name' => 'required|unique:companies,name,' . $company->id,
            ] );
            if ( $validation instanceof Response ) {
                return $validation;
            }

            $company->update( $request->only( [
                'name'
            ] ) );
            return $this->apiResponse( new CompanyResource( $company ), ApiController::STATUS_OK, 'updated successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function softDelete( $id ) {
        $company = Company::find( $id );
        if ( $company ) {
            $company->delete();
            return $this->apiResponse( new CompanyResource( $company ), ApiController::STATUS_OK, 'Company soft-delete successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function restore( $id ) {
        $company = Company::onlyTrashed()->find( $id );
        if ( $company ) {
            $company->restore();
            return $this->apiResponse( new CompanyResource( $company ), ApiController::STATUS_OK, 'Company restore successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function checkCompanyBalance( $id ) {
        $company = Company::find( $id );
        if ( $company ) {
            $payments_of_company = Payment::where( 'company_id', $id )->get();
            if ( count( $payments_of_company )>0 ) {
                $total = Payment::where( 'company_id', $id )->sum( 'amount' );
                $company->setAttribute( 'total_payments', $total );
                return $this->apiResponse( new CompanyBalanceResource( $company ), ApiController::STATUS_OK, 'Company Payments have been retrieved successfully' );
            }
            return $this->apiResponse( new CompanyResource( $company ), ApiController::STATUS_OK, 'this company has no payments' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Company not fount' );
    }

    public function showContractsOfCompany( $id ) {
        $company = Company::find( $id );
        if ( $company ) {
            $clients_of_company = Client::where( 'company_id', $id )->pluck( 'id' )->toArray();
            if ( count( $clients_of_company ) > 0 ) {
                $contracts = Contract::whereIn( 'client_id', $clients_of_company )->where( 'approved', 1 )->get();
                return $this->apiResponse( ContractResource::collection( $contracts ), ApiController::STATUS_OK, 'Contracts have been retrieved successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'There is no clients for this company' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Company not found' );
    }

    public function CompanyPayments( $id ) {

        $company = Company::find( $id );
        if ( $company ) {
            return $this->apiResponse( PaymentResource::collection( $company->Payments ), ApiController::STATUS_OK, 'Company Payments have been retrieved successfully' );

        }

    }
}
