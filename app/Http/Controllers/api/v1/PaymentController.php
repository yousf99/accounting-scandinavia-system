<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Payment;
use App\Http\Resources\PaymentResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends ApiController
 {
    public function store( Request $request ) {
        $validation = $this->apiValidation( $request, [
            'amount' => 'required',
            'company_id' => 'required|exists:companies,id',
            'description' => 'required',
            'type' => 'required|in:Cash,Bank',
        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }

        if ( $request->date )
        $date = $request->date;
        else
        $date = date( 'y-m-d' );

        $payment = Payment::create( array_merge( $request->only( [
            'amount', 'company_id', 'description', 'type'
        ] ), [ 'date' => $date ] ) );
        return $this->apiResponse( new PaymentResource( $payment ), ApiController::STATUS_CREATED, 'Added successfully' );
    }

    public function show( $id ) {
        $payment = Payment::find( $id );
        if ( $payment ) {
            return $this->apiResponse( new PaymentResource( $payment ), ApiController::STATUS_OK, 'Payment have been retrieved successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'There are no data' );
    }

    public function update( Request $request, $id ) {

        $validation = $this->apiValidation( $request, [
            'description' => 'required',

        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }

        $payment = Payment::find( $id );
        if ( $payment ) {

            if ( $request->date ) {
                $date = $request->date;
            } else {
                $date = date( 'y-m-d' );
            }

            $payment->date = $date;
            $payment->description = $request->description;
            $payment->update();
            return $this->apiResponse( new PaymentResource( $payment ), ApiController::STATUS_OK, 'Updated successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function softDelete( $id ) {
        $payment = Payment::find( $id );
        if ( $payment ) {
            $payment->delete();
            return $this->apiResponse( new PaymentResource( $payment ), ApiController::STATUS_OK, 'Payment soft-delete successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function restore( $id ) {
        $payment = Payment::onlyTrashed()->find( $id );
        if ( $payment ) {
            $payment->restore();
            return $this->apiResponse( new PaymentResource( $payment ), ApiController::STATUS_OK, 'Payment restore successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }
}
