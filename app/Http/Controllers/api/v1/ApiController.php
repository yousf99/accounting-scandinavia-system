<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\RestfulTrait;
use Illuminate\Support\Facades\Storage;
use Twilio\Rest\Client;
use App\Models\Category;
use App\Models\Notification;
use App\Models\UserDevice;
use App\Http\Resources\NotificationResource;

class ApiController extends Controller {
    use RestfulTrait;

    public const STATUS_OK = 200;
    public const STATUS_CREATED = 201;
    public const STATUS_NO_CONTENT = 204;
    public const STATUS_RESET_CONTENT = 205;

    //Exception
    public const STATUS_BAD_REQUEST = 400;
    public const STATUS_UNAUTHORIZED = 401;
    public const STATUS_NOT_AUTHENTICATED = 402;
    public const STATUS_FORBIDDEN = 403;
    public const STATUS_NOT_FOUND = 404;
    public const STATUS_VALIDATION = 405;

    public function upload_image( Request $request ) {
        $file = $request->file( 'image' );
        $extension = $file->getClientOriginalExtension();
        // getting image extension
        $filename = time().'.'.$extension;
        $imagePath = $request->file( 'image' )->store( 'images', 'public' );
        return  $imagePath;
    }

    public function Delete_image( $image ) {
        if ( file_exists( storage_path( 'app/public/' ).$image ) and $image != 'images/user_default.png' and $image != 'images/user_default.jpg' ) {
            Storage::disk( 'public' )->delete( $image );

        }

    }

    public function upload_file( Request $request ) {
        $file = $request->file( 'pdf' );
        $extension = $file->getClientOriginalExtension();
        // getting file extension
        $filename = time().'.'.$extension;
        $filePath = $request->file( 'pdf' )->store( 'files', 'public' );
        return  $filePath;

    }

    // public function upload_file( Request $request ) {
    //     $file = $request->file( 'file' );
    //     $extension = $file->getClientOriginalExtension();
    //     // getting file extension
    //     $filename = time().'.'.$extension;
    //     $filePath = $request->file( 'file' )->store( 'files', 'public' );
    //     return  $filePath;

    // }

    // public function sendMessage( $phoneNaumber, $code ) {
    //     $message = 'Your Security Code is '. $code;

    //     // try {

    //     $account_sid = getenv( 'TWILIO_SID' );
    //     $auth_token = getenv( 'TWILIO_TOKEN' );
    //     $twilio_number = getenv( 'TWILIO_FROM' );

    //     $client = new Client( $account_sid, $auth_token );
    //     $client->messages->create( $phoneNaumber, [
    //         'from' => $twilio_number,
    //         'body' => $message ] );

    //         //     // dd( 'SMS Sent Successfully.' );

    //         // } catch ( Exception $e ) {
    //         //     dd( 'Error: '. $e->getMessage() );
    //         // }
    //     }
    // public function SendUserNotification( $userID, $titleAR, $titleEN, $bodyAR, $bodyEN ) {

    //     $notification = Notification::create( [
    //         'title:ar'=> $titleAR,
    //         'title:en'=> $titleEN,
    //         'body:ar'=> $bodyAR,
    //         'body:en'=> $bodyEN,
    //         'user_id'=> $userID,
    // ] );
    //     $userDevices = UserDevice::where( 'user_id', $userID )->get();

    //     foreach ( $userDevices as $userDevice ) {
    //         $this->Notification( $userDevice->token, $titleEN, $bodyEN );
    //     }

    //     return $this->apiResponse( new NotificationResource( $notification ), ApiController::STATUS_CREATED, 'Added successfully' );

    // }

    // public function Notification( $Devicetoken, $MessageTitle, $MessageBody ) {
    //     $token = $Devicetoken;
    //     $from = 'AAAAZgkuWes:APA91bE3rKfKTS8MHwl5j-0dv0UeAWRHLP3_nz7hjX4IFJj_v1FmaMlRh4Ct-coSsL9GWCB0HSs0Tn8U35Vzsu9vQOCpZJfSnwH8EXA7C-T_sVSjr-VdWgYh_gtzxbih2MuP31lh6NFT';
    //     $msg = array
    //     (
    //         'body'  => $MessageBody,
    //         'title' => $MessageTitle,
    //         'receiver' => 'Users',
    //         'icon'  => 'https://image.flaticon.com/icons/png/512/270/270014.png', /*Default Icon*/
    //         'sound' => 'mySound'/*Default sound*/
    // );

    //     $fields = array
    //     (
    //         'to'        => $token,
    //         'notification'  => $msg
    // );

    //     $headers = array
    //     (
    //         'Authorization: key=' . $from,
    //         'Content-Type: application/json'
    // );
    //     //#Send Reponse To FireBase Server
    //     $ch = curl_init();
    //     curl_setopt( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    //     curl_setopt( $ch, CURLOPT_POST, true );
    //     curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
    //     curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    //     curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    //     curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
    //     $result = curl_exec( $ch );
    //     curl_close( $ch );
    // }

}
