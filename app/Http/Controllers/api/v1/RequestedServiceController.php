<?php

namespace App\Http\Controllers\api\v1;

use App\Models\RequestedService;
use App\Models\Contract;
use App\Models\Attachment;
use App\Http\Resources\RequestedServiceResource;
use App\Http\Resources\AttachmentResource;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;
use Carbon\Carbon;

class RequestedServiceController extends ApiController
 {

    public function show( $id ) {
        $service = RequestedService::find( $id );
        if ( $service ) {
            return $this->apiResponse( new RequestedServiceResource( $service ), ApiController::STATUS_OK, 'Service have been retrieved successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'There are no data' );
    }
    //add service to proposal

    public function store( Request $request ) {
        $validation = $this->apiValidation( $request, [
            'contract_id' => 'required|exists:contracts,id',
            'service_id' => 'required|exists:services,id',
            'price' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'duration_type' => 'required|in:monthly,annual,daily,one',
        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }

        $proposal = Contract::where( 'id', $request->contract_id )->where( 'approved', null )->first();
        if ( $proposal ) {
            $services_of_proposal = RequestedService::where( 'contract_id', $request->contract_id )->where( 'service_id', $request->service_id )->first();
            if ( $services_of_proposal ) {
                return $this->apiResponse( null, ApiController::STATUS_BAD_REQUEST, 'This service already in this proposal' );
            }
            if($request->withTax){
                $priceWithoutTax = $request->price - ((5*$request->price)/100);
            }
            $service = RequestedService::create(array_merge( $request->only( [
                'contract_id', 'service_id',  'duration', 'description', 'duration_type'
            ] ),[ 'price' => $priceWithoutTax ] ) );
            return $this->apiResponse( new RequestedServiceResource( $service ), ApiController::STATUS_CREATED, 'Added successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_BAD_REQUEST, 'not added' );
    }

    //Update service of proposal

    public function update( Request $request, $id ) {
        $requested_service = RequestedService::find( $id );
        if ( $requested_service ) {
            $validation = $this->apiValidation( $request, [
                // 'contract_id' => 'required',
                'price' => 'required',
                'description' => 'required',
                'duration' => 'required',
                'duration_type' => 'required|in:monthly,annual,daily,one'
            ] );
            if ( $validation instanceof Response ) {
                return $validation;
            }
            $proposal = Contract::where( 'id', $requested_service->contract_id )->first();
            if ( $proposal->approved === null ) {
                $requested_service->update( $request->only( [
                    'price', 'description', 'duration', 'duration_type'
                ] ) );
                return $this->apiResponse( new RequestedServiceResource( $requested_service ), ApiController::STATUS_OK, 'Updated successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_VALIDATION, 'Can not edit this requested services' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    //delete service from proposal

    public function destroy( $id ) {
        $service = RequestedService::withTrashed()->where( 'id', $id )->first();
        if ( $service ) {
            $proposal = Contract::where( 'id', $service->contract_id )->where( 'approved', null )->first();
            if ( $proposal ) {
                $service->forceDelete();
                return $this->apiResponse( null, ApiController::STATUS_OK, 'deleted successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_VALIDATION, 'can not deleted service from rejected proposal or contract' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function start( $id ) {
        $service = RequestedService::find( $id );
        if ( $service ) {
            $date = date( 'y-m-d' );
            $service->start_date = $date;
            $service->update();
            $this->SetNext( $service );
            $this->pdfInvoice( $id );

            return $this->apiResponse( new RequestedServiceResource( $service ), ApiController::STATUS_OK, 'Service Started Succesfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function SetNext( $service ) {

        if ( $service->duration_type == 'one' ) {
            $service->end_date = $service->start_date ;
            $service->finished = 1;
        }

        if ( $service->duration_type == 'monthly' ) {
            $service->next_date = Carbon::createFromFormat( 'y-m-d', $service->start_date )->addMonths( 1 )->format( 'y-m-d' );
            $service->end_date = Carbon::createFromFormat( 'y-m-d', $service->start_date )->addMonths( $service->duration )->format( 'y-m-d' );
        }

        if ( $service->duration_type == 'annual' ) {
            $service->next_date = Carbon::createFromFormat( 'y-m-d', $service->start_date )->addDays( 1 )->format( 'y-m-d' );
            $service->end_date = Carbon::createFromFormat( 'y-m-d', $service->start_date )->addDays( $service->duration )->format( 'y-m-d' );
        }

        if ( $service->duration_type == 'daily' ) {
            $service->next_date = Carbon::createFromFormat( 'y-m-d', $service->start_date )->addYear( 1 )->format( 'y-m-d' );
            $service->end_date = Carbon::createFromFormat( 'y-m-d', $service->start_date )->addYear( $service->duration )->format( 'y-m-d' );
        }

        $service->update();

    }

    public function getAttachments( $id ) {

        $service = RequestedService::find( $id );
        if ( $service ) {

            return $this->apiResponse( AttachmentResource::collection( $service->Attachments ), ApiController::STATUS_OK, 'Service Started Succesfully' );

        }

    }

    public function pdfInvoice( $id ) {

        $invoice = $this->getInvoice( $id );
        if ( $invoice ) {
            view()->share( 'invoice', $invoice );
            $pdf = PDF::loadView( 'pdf.invoice', $invoice );
            $fileName = time().$id.'.pdf';
            file_put_contents( $fileName, $pdf->output() );
            $attachment = Attachment::create( [ 'url'=>$fileName, 'type'=>'PDF', 'requested_service_id'=>$id ] );

            return $pdf->download( $fileName );
        }
        return null;

    }

    public function getInvoice( $id ) {

        $service = RequestedService::find( $id );
        if ( $service ) {
            if ( !$service->start_date )
            return null;
            $arr = array();
            //company Details
            $arr[ 'companyName' ] = $service->Contracts->Client->Company->name;
            $arr[ 'companyAddress' ] = $service->Contracts->Client->Country->name;
            $arr[ 'companyPhone' ] = $service->Contracts->Client->mobile;
            $arr[ 'companyEmail' ] = $service->Contracts->Client->email;

            //Bill Info
            $arr[ 'serviceDate' ] = date( 'y-m-d' );
            $arr[ 'accountNo' ] = 'U-'. str_pad( ( string )$service->id, 4, '0', STR_PAD_LEFT );
            $arr[ 'companyID' ] = $service->Contracts->Client->Company->account_no;

            // Service Details
            $arr[ 'serviceName' ] = $service->Service->name;
            $arr[ 'serviceDescription' ] = $service->description;
            $arr[ 'servicePrice' ] = $service->price;
            $arr[ 'serviceTax' ] = ( $service->price*5 )/100;
            $arr[ 'serviceTotal' ] = $arr[ 'servicePrice' ]+ $arr[ 'serviceTax' ];

            return $arr ;

        }
        return null;

    }

    public function checkServeciesRequested() {
        $services = RequestedService::whereNull( 'finished' )->get();
        foreach ( $services as $service ) {
            //check duration and duration type
            $this->checkDurationService( $service->id );

        }
    }

    public function checkDurationService( $id ) {
        $service = RequestedService::find( $id );
        $next_date = Carbon::createFromFormat( 'y-m-d', $service->next_date );
        $now = Carbon::now();
        if ( $next_date <= $now ) {
            $this->pdfInvoice( $id );

            if ( $service->duration_type == 'monthly' )
            $date = Carbon::createFromFormat( 'y-m-d', $service->next_date )->addMonths( 1 )->format( 'y-m-d' );

            if ( $service->duration_type == 'annual' )
            $date = Carbon::createFromFormat( 'y-m-d', $service->next_date )->addDays( 1 )->format( 'y-m-d' );

            if ( $service->duration_type == 'daily' )
            $date = Carbon::createFromFormat( 'y-m-d', $service->next_date )->addYear( 1 )->format( 'y-m-d' );

            $service->next_date = $date;
            $service->update();

            $end_date = Carbon::createFromFormat( 'y-m-d', $service->end_date );
            $now = Carbon::now();
            if ( $end_date <= $now ) {
                $service->next_date = null;
                $service->finished = 1;
                $service->update();
            }
        }

    }
}
