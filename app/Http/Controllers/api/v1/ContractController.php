<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Contract;
use App\Models\Payment;

use App\Models\RequestedService;
use App\Http\Resources\ContractResource;
use App\Http\Resources\RequestedServiceResource;
use App\Http\Resources\ProposalResource;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ContractController extends ApiController
 {
    public function showContracts() {

        $contract = Contract::where( 'approved', 1 )->paginate(10);
        return $this->apiResponse( ContractResource::collection( $contract )->response()->getData(true), ApiController::STATUS_OK, 'Contracts have been retrieved successfully' );
    }

    public function showProposals() {
        $proposals = Contract::where( 'approved', '=', null )->orWhere( 'approved', '=', 0 )->get();
        return $this->apiResponse( ProposalResource::collection( $proposals ), ApiController::STATUS_OK, 'Proposals have been retrieved successfully' );
    }

    public function show( $id ) {
        $contract = Contract::find( $id );
        if ( $contract ) {
            if ( $contract->approved )
            return $this->apiResponse( new ContractResource( $contract ), ApiController::STATUS_OK, 'Contract retrived successfully' );

            return $this->apiResponse( new ProposalResource( $contract ), ApiController::STATUS_OK, 'Contract retrived successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Contract not found' );
    }

    public function approve( $id, Request $request ) {

        $validation = $this->apiValidation( $request, [
            'pdf' => 'required',

        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }
        $proposal = Contract::where( 'id', $id )->whereNull( 'approved' )->first();

        if ( count( $proposal->RequestedServices ) <= 0 ) {
            return $this->apiResponse( null, ApiController::STATUS_FORBIDDEN, 'Proposal Has No Services' );

        }
        if ( $proposal ) {
            foreach ( $proposal->RequestedServices as $service ) {

                $date = date( 'y-m-d' );
                $price = ( -1 )*$service->price;
                $payment = Payment::create( [
                    'amount'=>$price, 'company_id'=>$proposal->client->company_id, 'description'=> $service->service->name, 'date' => $date, 'type' => 'Service' ] );

                }

                //function in ApiController
                $file_path = $this->upload_file( $request );

                $proposal->approved = 1;
                $proposal->approved_pdf = $file_path;

                $proposal->update();
                return $this->apiResponse( new ContractResource( $proposal ), ApiController::STATUS_OK, 'Proposal Approved Successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Proposal Not Found' );
        }

        public function reject( Request $request, $id ) {
            $validation = $this->apiValidation( $request, [ 'reason' => 'required' ] );
            if ( $validation instanceof Response ) {
                return $validation;
            }

            $proposal = Contract::where( 'id', $id )->whereNull( 'approved' )->first();
            if ( $proposal ) {
                $proposal->approved = 0;
                $proposal->reason = $request->reason;
                $proposal->update();
                return $this->apiResponse( new ContractResource( $proposal ), ApiController::STATUS_OK, 'Proposal Rejected Successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Proposal Not Found' );
        }

        //Store Proposal

        public function store( Request $request ) {
            $validation = $this->apiValidation( $request, [
                'client_id' => 'required|exists:clients,id',
                'pdf' => 'required',
                'name' => 'required'

            ] );
            if ( $validation instanceof Response ) {
                return $validation;
            }

            //function in ApiController
            $file_path = $this->upload_file( $request );
            $proposal = Contract::create(
                array_merge(
                    $request->only( [
                        'name', 'client_id'
                    ] ),
                    [ 'pdf'=>$file_path ]
                )
            );
            return $this->apiResponse( new ProposalResource( $proposal ), ApiController::STATUS_CREATED, 'Added successfully' );
        }

        public function softDelete( $id ) {
            $proposal = Contract::find( $id );
            if ( $proposal and !$proposal->approved ) {
                $proposal->delete();
                return $this->apiResponse( new ProposalResource( $proposal ), ApiController::STATUS_OK, 'Proposal delete successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Proposal does not exist' );
        }

        public function restore( $id ) {
            $proposal = contract::onlyTrashed()->find( $id );
            if ( $proposal ) {
                $proposal->restore();
                return $this->apiResponse( new ProposalResource( $proposal ), ApiController::STATUS_OK, 'Proposal restore successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
        }

        public function contractServices( $id ) {
            $contract = Contract::find( $id );
            if ( $contract and $contract->approved === 1 ) {
                $requested_services = RequestedService::where( 'contract_id', $id )->get();
                return $this->apiResponse( RequestedServiceResource::collection( $requested_services ), ApiController::STATUS_OK, 'Services of contract retrived successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Contract Not Found' );
        }

        public function proposalServices( $id ) {
            $proposal = Contract::find( $id );
            if ( $proposal and !$proposal->approved ) {
                $requested_services = RequestedService::where( 'contract_id', $id )->get();
                return $this->apiResponse( RequestedServiceResource::collection( $requested_services ), ApiController::STATUS_OK, 'Services of proposal retrived successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Proposal not found' );
        }

        public function getContract() {

        }
    }
