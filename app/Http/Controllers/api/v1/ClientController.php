<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Client;
use App\Models\Contract;
use App\Models\Company;
use App\Models\RequestedService;
use App\Http\Resources\ClientResource;
use App\Http\Resources\RequestedServiceResource;
use App\Http\Resources\RequestedClientServicesResource;

use App\Http\Resources\ContractResource;
use App\Models\Country;
use Illuminate\Support\Facades\DB;

class ClientController extends ApiController
 {
    public function index() {
        $client = Client::all();
        return $this->apiResponse( ClientResource::collection( $client ), ApiController::STATUS_OK, 'Clients have been retrieved successfully' );
    }

    public function show( $id ) {
        $client = Client::find( $id );
        if ( $client ) {
            return $this->apiResponse( new ClientResource( $client ), ApiController::STATUS_OK, 'Client have been retrieved successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'There are no data' );
    }

    public function showContracts( $id ) {
        $client = Client::find( $id );
        if ( $client ) {
            $contracts = Contract::where( 'client_id', $id )->get();
            if ( $contracts ) {
                return $this->apiResponse( ContractResource::collection( $contracts ), ApiController::STATUS_OK, 'contracts retrived successfully' );
            }
            return $this->apiResponse( null, ApiController::STATUS_OK, 'This client has no contracts' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Client Not Found' );
    }

    public function store( Request $request ) {
        $validation = $this->apiValidation( $request, [
            'name' => 'required',
            'email' => 'required|unique:clients,email',
            'country_id' => 'required|exists:countries,id',
        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }


        if ( !$request->company_id ) {
            $user_id = auth( 'sanctum' )->user()->id;
            $company = Company::create( [ 'name' => $request->name, 'user_id' => $user_id ] );
            $country=Country::find($request->country_id);
            $rest = substr($country->name, 0, 3);
            $accountNo = $rest. str_pad( ( string )$company->id, 4, '0', STR_PAD_LEFT );
            $company->account_no = $accountNo;
            $company->update();
            $companyID= $company->id ;  
            
        }
        else{
            $companyID=$request->company_id;
        }

        $client = Client::create( array_merge( $request->only( [
            'name', 'email', 'country_id','phone', 'mobile', 'fax'
        ] ), [ 'company_id' => $companyID ] ) );

        return $this->apiResponse( new ClientResource( $client ), ApiController::STATUS_CREATED, 'Added successfully' );
    }

    public function update( Request $request, $id ) {
        $client = Client::find( $id );
        if ( $client ) {
            $validation = $this->apiValidation( $request, [
                'name' => 'required',
                'email' => 'required|unique:clients,email,' . $client->id,
                'company_id' => 'sometimes|exists:companies,id',
                'country_id' => 'required|exists:countries,id',

            ] );
            if ( $validation instanceof Response ) {
                return $validation;
            }
            $client->update( $request->only( [
                'name', 'email', 'country_id', 'company_id','phone', 'mobile', 'fax'
            ] ) );
            return $this->apiResponse( new ClientResource( $client ), ApiController::STATUS_OK, 'Updated successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function softDelete( $id ) {
        $client = Client::find( $id );
        if ( $client ) {
            $client->delete();
            return $this->apiResponse( new ClientResource( $client ), ApiController::STATUS_OK, 'Client soft-delete successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function restore( $id ) {
        $client = Client::onlyTrashed()->find( $id );
        if ( $client ) {
            $client->restore();
            return $this->apiResponse( new ClientResource( $client ), ApiController::STATUS_OK, 'Client restore successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function showServices( $id ) {

        $contracts = DB::table('contracts')
        ->join('requested_services', function ($join) use ($id) {
            $join->on('contracts.id', '=', 'requested_services.contract_id')->where('contracts.client_id', '=',$id)->where('contracts.approved', '=', 1);
        })
        ->get();
        return $this->apiResponse( RequestedClientServicesResource::collection( $contracts ), ApiController::STATUS_OK, 'Services retrived successfully' );

    }
}
