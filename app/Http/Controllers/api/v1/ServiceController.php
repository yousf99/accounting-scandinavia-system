<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Service;
use App\Models\Contract;
use App\Models\Client;
use App\Models\RequestedService;
use App\Http\Resources\ServiceResource;
use App\Http\Resources\ClientResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Illuminate\Support\Facades\DB;

class ServiceController extends ApiController
 {

    public function index() {
        $service = Service::paginate(10);
        return $this->apiResponse( ServiceResource::collection( $service )->response()->getData(true), ApiController::STATUS_OK, 'Services have been retrieved successfully' );
    }

    public function show( $id ) {
        $service = Service::find( $id );
        if ( $service ) {
            return $this->apiResponse( new ServiceResource( $service ), ApiController::STATUS_OK, 'service have been retrieved successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'There are no data' );
    }

    public function store( Request $request ) {
        $validation = $this->apiValidation( $request, [
            'name' => 'required|unique:services,name,NULL,id,deleted_at,NULL',
            'description' => 'required',
        ] );
        if ( $validation instanceof Response ) {
            return $validation;
        }

        $service = Service::create( $request->only( [
            'name', 'description'
        ] ) );
        return $this->apiResponse( new ServiceResource( $service ), ApiController::STATUS_CREATED, 'Added successfully' );
    }

    public function update( Request $request, $id ) {

        $service = Service::find( $id );
        if ( $service ) {
            $validation = $this->apiValidation( $request, [
                'name' => 'required|unique:services,name,' . $service->id,
                'description' => 'required',
            ] );
            if ( $validation instanceof Response ) {
                return $validation;
            }
            $service->update( $request->only( [
                'name', 'description'
            ] ) );
            return $this->apiResponse( new ServiceResource( $service ), ApiController::STATUS_OK, 'Updated successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function softDelete( $id ) {

        $service = Service::find( $id );
        if ( $service ) {
            $service->delete();
            return $this->apiResponse( new ServiceResource( $service ), ApiController::STATUS_OK, 'Service soft-delete successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function restore( $id ) {

        $service = Service::onlyTrashed()->find( $id );
        if ( $service ) {
            $service->restore();
            return $this->apiResponse( new ServiceResource( $service ), ApiController::STATUS_OK, 'Service restore successfully' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'This object does not exist' );
    }

    public function showSubscribers( $id ) {

        $service = Service::find( $id );
        if ( $service ) {
            $contract_id = ( $service->RequestedServices->pluck( 'contract_id' ) );
            if ( count( $contract_id ) > 0 ) {
                $clients_id = Contract::whereIn( 'id', $contract_id )->pluck( 'client_id' );
                $subscribers = Client::whereIn( 'id', $clients_id )->paginate(10);

                // return $this->apiResponse( ServiceResource::collection( $service )->response()->getData(true), ApiController::STATUS_OK, 'Services have been retrieved successfully' );

                return $this->apiResponse( ClientResource::collection( $subscribers )->response()->getData(true), ApiController::STATUS_OK, 'Subscribers retrived successfully' );
            }
            return $this->apiResponse( [], ApiController::STATUS_NOT_FOUND, 'There is no subscribers for this service' );
        }
        return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'Service not found' );
    }
}
