<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class UserController extends ApiController {

    public function index() {
        $users = User::all();
        return $this->apiResponse( UserResource::collection( $users ), ApiController::STATUS_OK, 'users have been retrieved successfully' );

    }

    public function showProfile( Request $request ) {
        $user_id = auth( 'sanctum' )->user()->id;
        $user = User::find( $user_id );
        if ( $user )
        return $this->apiResponse( new UserResource( $user ), ApiController::STATUS_OK, 'user have been retrieved successfully' );

        return $this->apiResponse( '', ApiController::STATUS_NOT_FOUND, 'There are no data' );
    }

    public function login( Request $request ) {
        $validate = $this->apiValidation( $request, [
            'email' => 'required|email',
            'password' => 'required|min:8',

        ] );
        if ( $validate instanceof Response ) return $validate;
        $user = User::where( 'email', $request->email )->first();

        if ( !$user || !Hash::check( $request->password, $user->password ) ) {
            return $this->apiResponse( null, ApiController::STATUS_NOT_FOUND, 'These credentials do not match our records.' );
        }

        $token = $user->createToken( 'account_token' )->plainTextToken;
        return $this->apiResponse( [ 'admin' => new UserResource( $user ), 'token' => $token ], ApiController::STATUS_OK, 'You have successfully logged in' );
    }

    public function logout( Request $request ) {

        $token = auth()->user()->tokens()->where( 'id', auth()->user()->currentAccessToken()->id );
        if ( $token->delete() ) {
            return $this->apiResponse( null, self::STATUS_OK, 'Signed out successfully' );
        }
        return $this->apiResponse( null, self::STATUS_BAD_REQUEST, 'There is something wrong on deleting the current token' );
    }

}
