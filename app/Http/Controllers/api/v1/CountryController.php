<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Country;
use App\Http\Resources\CountryResource;

class CountryController extends ApiController
{
    public function index()
    {
        $country = Country::all();
        return $this->apiResponse(CountryResource::collection($country), ApiController::STATUS_OK, 'Countries have been retrieved successfully');
    }

    public function store(Request $request)
    {
        $validation = $this->apiValidation($request, [
            'name' => 'required|unique:countries,name',
        ]);
        if ($validation instanceof Response) {
            return $validation;
        }

        $country = Country::create($request->only([
            'name'
        ]));
        return $this->apiResponse(new CountryResource($country), ApiController::STATUS_CREATED, 'Added successfully');
    }

    public function update(Request $request, $id)
    {
        $country = Country::find($id);
        if ($country) {
            $validation = $this->apiValidation($request, [
                'name' => 'required|unique:countries,name,' . $country->id,
            ]);
            if ($validation instanceof Response) {
                return $validation;
            }
            $country->update($request->only([
                'name'
            ]));
            return $this->apiResponse(new CountryResource($country), ApiController::STATUS_OK, 'Updated successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'This object does not exist');
    }

    public function softDelete($id)
    {
        $country = Country::find($id);
        if ($country) {
            $country->delete();
            return $this->apiResponse(new CountryResource($country), ApiController::STATUS_OK, 'Country soft-delete successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'This object does not exist');
    }

    public function restore($id)
    {
        $country = Country::onlyTrashed()->find($id);
        if ($country) {
            $country->restore();
            return $this->apiResponse(new CountryResource($country), ApiController::STATUS_OK, 'Country restore successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'This object does not exist');
    }
}
