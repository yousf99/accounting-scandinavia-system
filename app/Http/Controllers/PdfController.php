<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\RequestedService;
use Illuminate\Support\Facades\Storage;

class PdfController extends Controller {
    public function index() {
        return view( 'pdf.invoice' );

    }

    public function pdfInvoice( $id ) {

        $invoice = $this->getInvoice( $id );
        if ( $invoice ) {
            view()->share( 'invoice', $invoice );
            $pdf = PDF::loadView( 'pdf.invoice', $invoice );
            file_put_contents( 'Brochure.pdf', $pdf->output() );

            return $pdf->download( 'event-attendees.pdf' );
        }
        return null;

    }

    public function getInvoice( $id ) {

        $service = RequestedService::find( $id );
        if ( $service ) {
            if ( !$service->start_date )
            return null;
            $arr = array();
            //company Details
            $arr[ 'companyName' ] = $service->Contracts->Client->Company->name;
            $arr[ 'companyAddress' ] = $service->Contracts->Client->Country->name;
            $arr[ 'companyPhone' ] = $service->Contracts->Client->mobile;
            $arr[ 'companyEmail' ] = $service->Contracts->Client->email;

            //Bill Info
            $arr[ 'serviceDate' ] = $service->start_date;
            $arr[ 'accountNo' ] = 'U-'. str_pad( ( string )$service->id, 4, '0', STR_PAD_LEFT );
            $arr[ 'companyID' ] = $service->Contracts->Client->Company->account_no;

            // Service Details
            $arr[ 'serviceName' ] = $service->Service->name;
            $arr[ 'serviceDescription' ] = $service->description;
            $arr[ 'servicePrice' ] = $service->price;
            $arr[ 'serviceTax' ] = ( $service->price*5 )/100;
            $arr[ 'serviceTotal' ] = $arr[ 'servicePrice' ]+ $arr[ 'serviceTax' ];

            return $arr ;

        }
        return null;

    }

}
