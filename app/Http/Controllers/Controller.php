<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Mail;
use App\Mail\VerificationMail;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index() {

        return view( 'firebase' );
    }

    public function sendNotification() {
        $token = 'fXdGhW8xUGc:APA91bEi9eF_heC1JmIkRLJwlMQ6ejQ5H3eXUDfqDgQr9t22-h_4NxMNQBJK3jE2aH-k8IGsCHpOOjeG-NiBSJ_lhe4_enVU0rrgWgXM2l0o34gA_QnVrTzOOFgur8aIzmZ2T20y-4yd';
        $from = 'AAAAZgkuWes:APA91bE3rKfKTS8MHwl5j-0dv0UeAWRHLP3_nz7hjX4IFJj_v1FmaMlRh4Ct-coSsL9GWCB0HSs0Tn8U35Vzsu9vQOCpZJfSnwH8EXA7C-T_sVSjr-VdWgYh_gtzxbih2MuP31lh6NFT';
        $msg = array
        (
            'body'  => 'Testing Testing',
            'title' => 'Hi, From Rizek',
            'receiver' => 'erw',
            'icon'  => 'https://image.flaticon.com/icons/png/512/270/270014.png', /*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $fields = array
        (
            'to'        => $token,
            'notification'  => $msg
        );

        $headers = array
        (
            'Authorization: key=' . $from,
            'Content-Type: application/json'
        );
        //#Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec( $ch );
        dd( $result );
        curl_close( $ch );
    }

    // public function testMail() {
    //     $mail = 'yousef.aj3333@gmail.com';
    //     $digits = 6;
    //     $code = rand( pow( 10, $digits-1 ), pow( 10, $digits )-1 );

    //     Mail::to( $mail )->send( new VerificationMail ( $code ) );

    //     dd( 'Mail Send Successfully !!' );
    // }
}
