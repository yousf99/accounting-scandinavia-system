<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Contract extends Model
 {
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'client_id',
        'reason',
        'approved',
        'pdf',
        'approved_pdf',
    ];

    public function Client() {
        return $this->belongsTo( Client::class );
    }

    public function RequestedServices() {
        return $this->hasMany( RequestedService::class );
    }

    public  function getFilePath() {
        return asset( 'storage/'. $this->pdf );
    }

    public  function getFileContractPath() {
        return asset( 'storage/'. $this->approved_pdf );
    }

    public function getCreatedAtAttribute( $date ) {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $date )->format( 'Y-m-d' );
    }

    public function getUpdatedAtAttribute( $date ) {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $date )->format( 'Y-m-d' );
    }
}
