<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestedService extends Model
 {
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'contract_id', 'service_id', 'price', 'start_date', 'next_date', 'end_date', 'finished', 'duration', 'duration_type', 'description'
    ];

    protected $casts = [
        'price' => 'integer',
        'duration' => 'integer',

    ];

    public function Contracts() {
        return $this->belongsTo( Contract::class, 'contract_id' );
    }

    public function Service() {
        return $this->belongsTo( Service::class, 'service_id' );
    }

    public function Attachments() {
        return $this->hasMany( Attachment::class );
    }

}
