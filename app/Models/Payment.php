<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
 {
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'amount', 'company_id', 'date', 'description', 'type'
    ];

    protected $casts = [
        'amount' => 'integer',
    ];

    public function contract()
 {
        return $this->belongsTo( Company::class );
    }
}
