<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
 {
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'account_no',
        'user_id',
    ];

    public function clients()
 {
        return $this->hasMany( Client::class );
    }

    public function Payments()
 {
        return $this->hasMany( Payment::class );
    }

    public function Users()
 {
        return $this->belongsTo( User::class );
    }
}
