<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Attachment extends Model {
    use HasFactory;
    protected $fillable = [
        'url',
        'type',
        'requested_service_id',
    ];

    public function RequestedServices() {
        return $this->belongsTo( RequestedService::class, 'requested_service_id' );
    }

    public  function getFilePath() {
        return asset( 'storage/'. $this->url );
    }

    public function getCreatedAtAttribute( $date ) {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $date )->format( 'Y-m-d' );
    }
}
