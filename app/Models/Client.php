<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
 {
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'company_id',
        'country_id',
        'phone',
        'mobile',
        'fax',
    ];

    public function Company()
 {
        return $this->belongsTo( Company::class, 'company_id' );
    }

    public function Country()
 {
        return $this->belongsTo( Country::class, 'country_id' );
    }

    public function Contracts()
 {
        return $this->hasMany( Contract::class );
    }
}
