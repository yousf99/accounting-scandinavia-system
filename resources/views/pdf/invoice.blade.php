<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
      body {
      }
      page {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
      }
      page[size="A4"] {
        width: 21cm;
        height: 29.7cm;
      }
      page[size="A4"][layout="landscape"] {
        width: 29.7cm;
        height: 21cm;
      }
      page[size="A3"] {
        width: 29.7cm;
        height: 42cm;
      }
      page[size="A3"][layout="landscape"] {
        width: 42cm;
        height: 29.7cm;
      }
      page[size="A5"] {
        width: 14.8cm;
        height: 21cm;
      }
      page[size="A5"][layout="landscape"] {
        width: 21cm;
        height: 14.8cm;
      }
      @media print {
        body,
        page {
          margin: 0;
          box-shadow: 0;
        }
      }

      /*my css*/
      .title {
        color: #08588e;
        font-size: 2.7em;
        font-weight: 600;
      }

      .img-none {
        opacity: 0;
      }

      .tr-head {
        background-color: #08588e;
        color: white;
      }
      /* .table-one {
        margin-left: 50px;
        margin-right: 50px;
      } */
      .mx-auto {
        margin-bottom: 10px;
        width: 90%;
        margin-top: 10px;
        line-height: 1.3em;
      }
      .tt td{
        border: rgb(195, 195, 195) 1px solid;
        margin: 0;
        padding-top: 10px;

      }

      .tt th{
        padding-top: 10px;

        border: #08588e 1px solid;

      }
      table th{
        padding-top: 10px;}

    </style>
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
      tailwind.config = {
        theme: {
          extend: {
            colors: {
              clifford: "#da373d",
            },
          },
        },
      };
    </script>
  </head>
  <body>

    <table> 
        <tr>
          <td>
            <a href="https://scandinaviatech.com/">
                <img width="100"src="http://localhost/accounting/public/storage/files/logo.png" />
              </a>
          </td>
        </tr>
        <tr>
            <td>
                <p class="title">TAX INVOICE</p>
            </td>
        </tr>
    </table>




      <div class="h-full">
        <!--Scandinavia tech info-->
        <div class="table-one">
          <table class="mx-auto font-mono w-full mb-6">
            <thead class="text-gray-800 bg-gray-200">
              <tr class="h-10 tr-head">
                <th>Scandinavia Tech</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              <tr class="h-10">
                <td>
                  <div class="text-center">Address</div>
                </td>
                <td>
                  <div class="text-left">
                    The Metropolis tower office 1814 Business Bay, Dubai, UAE.
                  </div>
                </td>
              </tr>
              <tr class="h-10">
                <td>
                  <div class="text-center">Phone</div>
                </td>
                <td>
                  <a href="tel:+971 562 905 588">+971 562 905 588</a>
                </td>
              </tr>
              <tr class="h-10">
                <td>
                  <div class="text-center">Email</div>
                </td>
                <td>
                  <div class="text-left">
                    <a href="mailto:accounting@scandinaviatech.com">
                      accounting@scandinaviatech.com
                    </a>
                  </div>
                </td>
              </tr>
              <tr class="h-10">
                <td>
                  <div class="text-center" >TRN</div>
                </td>
                <td>
                  <div class="text-left">100314252600003</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!--To & bill info-->
        <div>
          <table> 
            <tr>
              <td>
                <div class="table-one">
                  <table style="margin-right: 150px"  class="tt mx-auto font-mono w-full mb-6 ">
                    <thead class="text-gray-800 bg-gray-200">
                      <tr class="h-10 tr-head">
                        <th>To</th>
                        <th> {{ $invoice['companyName'] }}</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr class="h-10">
                        <td class="">
                          <div
                            class="text-center"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                            Address
                          </div>
                        </td>
                        <td class="">
                          <div
                            class="text-left"
                            style="margin-left: 10px; margin-right: 10px"
                          >

                          {{ $invoice['companyAddress'] }}
                          </div>
                        </td>
                      </tr>
                      <tr class="h-10">
                        <td class="">
                          <div
                            class="text-center"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                            Phone
                          </div>
                        </td>
                        <td class="">
                          <div
                            class="text-left"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                          {{ $invoice['companyPhone'] }}
                          </div>
                        </td>
                      </tr>
                      <tr class="h-10">
                        <td class="">
                          <div
                            class="text-center"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                            Email
                          </div>
                        </td>
                        <td class="">
                          <div
                            class="text-left"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                          {{ $invoice['companyEmail'] }}
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </td>
              <td>
                <div class="table-one">
                  <table    class=" tt mx-auto font-mono w-full mb-6">
                    <tbody>
                      <tr class="h-10">
                        <td class=" border-t">
                          <div
                            class="text-left"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                            date:
                          </div>
                        </td>
                        <td class=" border-t">
                          <div class="text-center">{{ $invoice['serviceDate'] }}</div>
                        </td>
                      </tr>
                      <tr class="h-10">
                        <td class="">
                          <div
                            class="text-left"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                            Bill#:
                          </div>
                        </td>
                        <td class="">
                          <div
                            class="text-center"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                          {{ $invoice['accountNo'] }}
                          </div>
                        </td>
                      </tr>
                      <tr class="h-10">
                        <td class="">
                          <div
                            class="text-left"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                            Customer ID:
                          </div>
                        </td>
                        <td class="">
                          <div
                            class="text-center"
                            style="margin-left: 10px; margin-right: 10px"
                          >
                          {{ $invoice['companyID'] }}
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </td>
            </tr>
          </table>
        </div>

        <!-- Bill -->
        <div class="table-one">
          <table  class="tt mx-auto font-mono w-full mb-6">
            <thead class="text-gray-800 bg-gray-200">
              <tr class="h-10 tr-head">
                <th class="">Description</th>
                <th class="">Service</th>
                <th class="">Unit Price</th>
                <th class="">Total</th>
              </tr>
            </thead>

            <tbody>
              <tr class="border-r  h-10">
                <td class="">
                  <div class="text-center">{{ $invoice['serviceDescription'] }}</div>
                </td>
                <td class="">
                  <div class="text-center">{{ $invoice['serviceName'] }}</div>
                </td>
                <td class="">
                  <div class="text-center">{{ $invoice['servicePrice'] }}</div>
                </td>
                <td class="">
                  <div class="text-center">{{ $invoice['servicePrice'] }}</div>
                </td>
              </tr>

              <tr class="border-r  border-t h-10">
                <td class=" tr-head">
                  <div class="text-center">Notes or Special Comments:</div>
                </td>
                <td class="">
                  <div class="text-center">Sub Total</div>
                </td>
                <td class="" colspan="2">
                  <div class="text-center">AED {{ $invoice['servicePrice'] }}</div>
                </td>
              </tr>

              <tr class="border-r  border-t h-10">
                <td rowspan="2" class="">
                  <div class="text-center"></div>
                </td>
                <td class="">
                  <div class="text-center">Tax 5%</div>
                </td>
                <td colspan="2" class="">
                  <div class="text-center">AED {{ $invoice['serviceTax'] }}</div>
                </td>
              </tr>

              <tr class="border-r  border-t h-10">
                <td class="">
                  <div class="text-center">Balance Due:</div>
                </td>
                <td colspan="2" class="">
                  <div class="text-center">AED {{ $invoice['serviceTotal'] }}</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!--Bank Info-->
        <div class="table-one">
          <table>
            <tr>
              <td>
                <table>
                  <tr class="h-10">
                    <td>SCANDINAV TECHNOLOGY</td>
                  </tr>
                  <tr>
                    <td>Abu Dhabi commercial bank</td>
                  </tr>
                  <tr>
                    <td>Account No: 12016832920001</td>
                  </tr>
                  <tr>
                    <td>Branch: AL RIGGA ROAD</td>
                  </tr>
                  <tr>
                    <td>SWIFT CODE: ADCBAEAA</td>
                  </tr>
                </table>
              </td>
              <td>
                <img  width="100"  src="http://localhost/accounting/public/storage/files/ADCB.png" class="w-32" />

              </td>
              <td>
          <img  width="150" style="" src="http://localhost/accounting/public/storage/files/dubai.jpg" class="w-32" />

              </td>
            </tr>
          </table>
          <div>
            <p>If you have any questions about this invoice, please contact</p>
          </div>
          <div>
            <a href="mailto:accounting@Scandinaviatech.com">
              <p>Accounting@Scandinaviatech.com</p>
            </a>
          </div>
        </div>
      </div>
  </body>
</html>
