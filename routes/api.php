<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Auth
Route::prefix('auth')->group(function () {
    Route::post('login', [v1\UserController::class, 'login']);
});

Route::prefix('admin')->middleware(['auth:sanctum', 'permission:admin'])->group(function () {
    Route::apiResource('users', v1\UserController::class)->only('index', 'update', 'store');
    Route::get('profile', [v1\UserController::class, 'showProfile']);
    Route::post('logout', [v1\UserController::class, 'logout']);

    //Client
    Route::apiResource('clients', v1\ClientController::class)->only('index', 'update', 'store');
    Route::controller(v1\ClientController::class)->prefix('client')->group(function () {
        Route::get('show/{id}', 'show');
        Route::delete('destroy/{id}', 'destroy');
        Route::delete('delete/{id}', 'softDelete');
        Route::post('restore/{id}', 'restore');
    });

    //Service
    Route::apiResource('services', v1\ServiceController::class)->only('index', 'update', 'store');
    Route::prefix('service')->group(function () {
        Route::get('show/{id}', [v1\ServiceController::class, 'show']);
        Route::get('show_subscribers/{id}', [v1\ServiceController::class, 'showSubscribers']);
        Route::delete('delete/{id}', [v1\ServiceController::class, 'softDelete']);
        Route::post('restore/{id}', [v1\ServiceController::class, 'restore']);
    });

    //Contract
    Route::get('show_contracts', [v1\ContractController::class, 'showContracts']);
    Route::prefix('contract')->group(function () {
        Route::get('show/{id}', [v1\ContractController::class, 'show']);
        Route::get('contract_services/{id}', [v1\ContractController::class, 'contractServices']);
    });

    //Proposal
    Route::apiResource('proposals', v1\ContractController::class)->only('store');
    Route::get('show_proposals', [v1\ContractController::class, 'showProposals']);
    Route::prefix('proposal')->group(function () {
        Route::get('show/{id}', [v1\ContractController::class, 'show']);
        Route::delete('delete/{id}', [v1\ContractController::class, 'softDelete']);
        Route::post('restore/{id}', [v1\ContractController::class, 'restore']);
        Route::post('approve/{id}', [v1\ContractController::class, 'approve']);
        Route::put('reject/{id}', [v1\ContractController::class, 'reject']);
        Route::get('proposal_services/{id}', [v1\ContractController::class, 'proposalServices']);
    });

    //Company
    Route::apiResource('companies', v1\CompanyController::class)->only('index', 'update', 'store');
    Route::prefix('company')->group(function () {
        Route::get('show/{id}', [v1\CompanyController::class, 'show']);
        Route::get('check_company_balance/{id}', [v1\CompanyController::class, 'checkCompanyBalance']);
        Route::get('company_payments/{id}', [v1\CompanyController::class, 'CompanyPayments']);
        Route::get('company_contracts/{id}', [v1\CompanyController::class, 'showContractsOfCompany']);
        Route::delete('delete/{id}', [v1\CompanyController::class, 'softDelete']);
        Route::post('restore/{id}', [v1\CompanyController::class, 'restore']);
    });

    //Country
    Route::apiResource('countries', v1\CountryController::class)->only('index', 'update', 'store');
    Route::prefix('country')->group(function () {
        Route::delete('delete/{id}', [v1\CountryController::class, 'softDelete']);
        Route::post('restore/{id}', [v1\CountryController::class, 'restore']);
    });

    //Client
    Route::apiResource('clients', v1\ClientController::class)->only('index', 'update', 'store');
    Route::prefix('client')->group(function () {
        Route::get('show/{id}', [v1\ClientController::class, 'show']);
        Route::get('show_contracts/{id}', [v1\ClientController::class, 'showContracts']);
        Route::get('show_services/{id}', [v1\ClientController::class, 'showServices']);
        Route::delete('delete/{id}', [v1\ClientController::class, 'softDelete']);
        Route::post('restore/{id}', [v1\ClientController::class, 'restore']);
    });

    //Payment
    Route::apiResource('payments', v1\PaymentController::class)->only('store', 'update');
    Route::prefix('payment')->group(function () {
        Route::get('show/{id}', [v1\PaymentController::class, 'show']);
        Route::delete('delete/{id}', [v1\PaymentController::class, 'softDelete']);
        Route::post('restore/{id}', [v1\PaymentController::class, 'restore']);
    });


    //Requested Service
    Route::apiResource('requested_services', v1\RequestedServiceController::class)->only('store', 'update');
    Route::prefix('requested_service')->group(function () {
        Route::get('show/{id}', [v1\RequestedServiceController::class, 'show']);
        Route::delete('destroy/{id}', [v1\RequestedServiceController::class, 'destroy']);
        Route::post('start/{id}', [v1\RequestedServiceController::class, 'start']);
        Route::get('attachments/{id}', [v1\RequestedServiceController::class, 'getAttachments']);
        Route::post('check', [v1\RequestedServiceController::class, 'checkServeciesRequested']);
    });

        Route::get('/print/{id}', [v1\RequestedServiceController::class, 'pdfInvoice']);
    
});
