<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_services', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contract_id')->unsigned();
            $table->unsignedBigInteger('service_id')->unsigned();
            $table->foreign('contract_id')->references('id')->on('contracts')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->double('price');
            $table->string('description');
            $table->string('start_date')->nullable();
            $table->string('next_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('finished')->nullable();
            $table->integer('duration');
            $table->string('duration_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_services');
    }
};
