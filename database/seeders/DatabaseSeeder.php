<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
 {

    public function run()
 {
        $this->call( [
            UserSeeder::class,
            CompanySeeder::class,
            CountrySeeder::class,
            ClientSeeder::class,
            ServiceSeeder::class,
            ContractSeeder::class,
            PaymentSeeder::class,
            RequestedServiceSeeder::class,
            AttachmentSeeder::class,

            // ContractServiceSeeder::class
        ] );
    }
}
