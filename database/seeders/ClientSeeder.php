<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    public function run()
    {
        //Clients of company 1
        $client = Client::create([
            'name' => 'ahmed',
            'email' => 'ahmed.x@gmail.com',
            'company_id' => 1,
            'country_id' => 2,
        ]);
        $client = Client::create([
            'name' => 'salah',
            'email' => 'salah.tesla@gmail.com',
            'company_id' => 1,
            'country_id' => 2,
        ]);
        $client = Client::create([
            'name' => 'anas',
            'email' => 'anoos.f@gmail.com',
            'company_id' => 1,
            'country_id' => 3,
        ]);

        //Clients of company 2
        $client = Client::create([
            'name' => 'khaled',
            'email' => 'kh.x@gmail.com',
            'company_id' => 2,
            'country_id' => 1,
        ]);
        $client = Client::create([
            'name' => 'basel',
            'email' => 'basel.bw@gmail.com',
            'company_id' => 2,
            'country_id' => 1,
        ]);

        //Free Clients
        $client = Client::create([
            'name' => 'mohamed',
            'email' => 'm7mad.l@gmail.com',
            'country_id' => 3,
        ]);
        $client = Client::create([
            'name' => 'zaid',
            'email' => 'zd.hr@gmail.com',
            'country_id' => 3,
        ]);
    }
}
