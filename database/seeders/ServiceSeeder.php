<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServiceSeeder extends Seeder
 {
    public function run()
 {
        $service = Service::create( [
            'name' => 'Marketing',
            'description' => 'Marketing with Scandinavia-tech',
        ] );

        $service = Service::create( [
            'name' => 'SEO',
            'description' => 'SEO with Scandinavia-tech',
        ] );

        $service = Service::create( [
            'name' => 'Wordpress',
            'description' => 'Wordpress with Scandinavia-tech',
        ] );
    }
}
