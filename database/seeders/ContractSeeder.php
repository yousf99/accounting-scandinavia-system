<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contract;

class ContractSeeder extends Seeder
 {
    public function run()
 {
        Contract::create( [
            'name' => 'Marketing with youssef Company',
            'client_id' => 1,
            'approved' => 1,
            'pdf' => 'https://localhost',

        ] );

        Contract::create( [
            'name' => 'Scandinaviatech Site with youssef Company',
            'client_id' => 2,
            'pdf' => 'https://localhost',

        ] );

        Contract::create( [
            'name' => 'socialMedia Site with youssef Company',
            'client_id' => 1,
            'approved' => 0,
            'reason' => 'The customer does not interested about this offer',
            'pdf' => 'https://localhost',

        ] );
    }
}
