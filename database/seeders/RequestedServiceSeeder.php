<?php

namespace Database\Seeders;

use App\Models\RequestedService;
use Illuminate\Database\Seeder;

class RequestedServiceSeeder extends Seeder
{
    public function run()
    {
        RequestedService::create([
            'contract_id' => 1, 'service_id' => 3, 'price' => 1000,
            'duration' => 1, 'duration_type' => 'one', 'description' => 'test for the fucking test'
        ]);
        RequestedService::create([
            'contract_id' => 1, 'service_id' => 2, 'price' => 250,
            'duration' => 4, 'duration_type' => 'monthly', 'description' => 'test for the fucking test number 2'
        ]);

        // RequestedService::create(['contract_id' => 2, 'service_id' => 3, 'price' => 250, 'duration' => 1, 'duration_type' => 'one']);

        // RequestedService::create(['contract_id' => 3, 'service_id' => 1, 'price' => 5, 'duration' => 10, 'duration_type' => 'daily']);
        // RequestedService::create(['contract_id' => 3, 'service_id' => 2, 'price' => 300, 'duration' => 4, 'duration_type' => 'monthly']);
    }
}
