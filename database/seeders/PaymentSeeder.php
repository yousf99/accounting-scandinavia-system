<?php

namespace Database\Seeders;

use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
 {
    public function run()
 {
        Payment::create( [ 
            'amount' => 2000, 
            'description'=>'For Testing', 
            'company_id' => 1, 
            'date' => '22-09-02',
            'type'=>'Cash'
            ] );
        Payment::create( [ 'amount' => 1500, 'description'=>'For Testing', 'company_id' => 1, 'date' => '22-09-02','type'=>'Bank' ] );

        Payment::create( [ 'amount' => 1500, 'description'=>'For Testing', 'company_id' => 2, 'date' => '22-09-03','type'=>'Cash' ] );

        Payment::create( [ 'amount' => 100, 'description'=>'For Testing', 'company_id' => 3, 'date' => '22-09-02','type'=>'Bank' ] );
    }
}
