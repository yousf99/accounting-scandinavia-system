<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    public function run()
    {
        Company::create([
            'name' => 'CutsNGains',
            'account_no' => '00000000',
            'user_id' => '1',
        ]);

        Company::create([
            'name' => 'Royal Group',
            'account_no' => '00000001',
            'user_id' => '1',
        ]);

        Company::create([
            'name' => 'Alma Kitchen',
            'account_no' => '00000002',
            'user_id' => '2',
        ]);
    }
}
