<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Permission::create(['name' => 'admin']);

        $user = User::create([
            'name' => 'youssef Alfarra',
            'email' => 'youssefalfarra99@gmail.com',
            'password' => bcrypt('00000000'),
        ]);
        $user->givePermissionTo('admin');

        $user = User::create([
            'name' => 'Youssef Jamous',
            'email' => 'yousf.jamous@gmail.com',
            'password' => bcrypt('11111111'),
        ]);
        $user->givePermissionTo('admin');
    }
}
