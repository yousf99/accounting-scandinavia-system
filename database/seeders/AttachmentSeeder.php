<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Attachment;

class AttachmentSeeder extends Seeder {
    /**
    * Run the database seeds.
    *
    * @return void
    */

    public function run() {

        $type = 'image/jpg';
        $attachment = Attachment::create( [

            'url'=>'files/3KL6wFHUfmXTOQKswuJRhTfIbE8vsQ8vEQTIIyL0.pdf',
            'requested_service_id' => '1',
            'type' => $type,
        ] );

        $type = 'image/jpg';
        $attachment = Attachment::create( [

            'url'=>'files/3KL6wFHUfmXTOQKswuJRhTfIbE8vsQ8vEQTIIyL0.pdf',
            'requested_service_id' => '2',
            'type' => $type,
        ] );

        $type = 'image/jpg';
        $attachment = Attachment::create( [

            'url'=>'files/3KL6wFHUfmXTOQKswuJRhTfIbE8vsQ8vEQTIIyL0.pdf',
            'requested_service_id' => '2',
            'type' => $type,
        ] );

        $type = 'image/jpg';
        $attachment = Attachment::create( [

            'url'=>'files/3KL6wFHUfmXTOQKswuJRhTfIbE8vsQ8vEQTIIyL0.pdf',
            'requested_service_id' => '2',
            'type' => $type,
        ] );
    }
}
